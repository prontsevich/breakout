﻿using System.Linq;
using UnityEngine;

public class Block : MonoBehaviour
{
    [SerializeField]
    private int score = 10;

    [SerializeField]
    private bool isUnbreakable;

    [SerializeField]
    private bool isInvisible;

    [SerializeField]
    private Sprite[] sprites;

    [SerializeField]
    private bool isExploding;

    [SerializeField]
    private int explodingRadius;

    [SerializeField]
    private Pickup pickup;

    [SerializeField]
    private GameObject explodeParticles;

    [SerializeField]
    private AudioClip destroySound;

    private ScoreManager scoreManager;
    private SpriteRenderer spriteRenderer;
    private LevelManager levelManager;
    private GameManager gameManager;

    private int health = 1;

    public bool IsUnbreakable()
    {
        return isUnbreakable;
    }

    public void DestroyBlock()
    {
        scoreManager.AddScore(score);
        levelManager.RemoveBlock();
        Destroy(gameObject);

        if (pickup != null)
        {
            Instantiate(pickup, transform.position, Quaternion.identity);
        }

        if (isExploding)
        {
            var objects = Physics2D.OverlapCircleAll(transform.position, explodingRadius, LayerMask.GetMask("Block"));
            if (explodeParticles)
            {
                Instantiate(explodeParticles, transform.position, Quaternion.identity);
            }

            foreach (var obj in objects)
            {
                obj.gameObject.GetComponent<Block>().DestroyBlock();
            }
        }
    }

    private void Start()
    {
        health = sprites.Length;
        scoreManager = FindObjectOfType<ScoreManager>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        gameManager = FindObjectOfType<GameManager>();

        if (isInvisible)
        {
            spriteRenderer.sprite = null;
        }
        levelManager = FindObjectOfType<LevelManager>();
        ChangeSprite();
    }

    private void TakeDamage()
    {
        if (!isUnbreakable)
        {
            health -= 1;
            if (health <= 0)
            {
                DestroyBlock();
            }
            else
            {
                ChangeSprite();
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        TakeDamage();
        gameManager.PlaySound(destroySound);
    }

    private void ChangeSprite()
    {
        if (sprites[sprites.Length - health] != null)
        {
            spriteRenderer.sprite = sprites[sprites.Length - health];
        }
    }

    private void OnDrawGizmos()
    {
        if (isExploding)
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(transform.position, explodingRadius);
        }
    }
}
