﻿using UnityEngine;

public class Floor : MonoBehaviour
{
    private GameManager gameManager;

    private void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Ball")
        {
            gameManager.WasteLife();
        }
        else
        {
            Destroy(collision.gameObject);
        }
    }
}
