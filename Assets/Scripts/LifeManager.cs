﻿using UnityEngine;

public class LifeManager : MonoBehaviour
{
    [SerializeField]
    int lifesCount;

    [SerializeField]
    GameObject lifePrefab;

    [SerializeField]
    Transform lifesContainer;

    public delegate void LifeAction();
    public static event LifeAction OnGameOver;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < lifesCount; i++)
        {
            AddLifeToContainer();
        }
    }

    public void AddLife(int count = 1)
    {
        for (var i = count; i > 0; i--)
        {
            lifesCount++;
            AddLifeToContainer();
        }
    }

    public void WasteLife(int count = -1)
    {
        for (var i = count; i < 0; i++)
        {
            lifesCount--;
            if (lifesCount <= 0)
            {
                OnGameOver?.Invoke();
                return;
            }
            else
            {
                RemoveLifeFromContainer();
            }
        }
    }

    private void AddLifeToContainer()
    {
        Instantiate(lifePrefab, lifesContainer);
    }

    private void RemoveLifeFromContainer()
    {
        Destroy(lifesContainer.GetChild(0).gameObject);
    }
}
