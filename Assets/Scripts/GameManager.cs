﻿using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [Header("UI")]
    [SerializeField]
    TextMeshProUGUI scoreTextComponent;

    [SerializeField]
    Canvas mainCanvas;

    [SerializeField]
    Image pauseImage;

    LifeManager lifeManager;
    ScoreManager scoreManager;
    SceneLoader sceneLoader;
    Ball ball;
    AudioSource audioSource;
    bool isGameOver;

    public bool isPause;

    public void WasteLife()
    {
        lifeManager.WasteLife();
        ResetBall();
    }

    public void PlaySound(AudioClip sound)
    {
        audioSource.PlayOneShot(sound);
    }

    public void ResetBall()
    {
        StartCoroutine(ball.ResetBall());
    }

    // Start is called before the first frame update
    private void Start()
    {
        sceneLoader = FindObjectOfType<SceneLoader>();
        scoreManager = FindObjectOfType<ScoreManager>();
        lifeManager = FindObjectOfType<LifeManager>();
        ball = FindObjectOfType<Ball>();
        audioSource = FindObjectOfType<AudioSource>();
        pauseImage.gameObject.SetActive(false);
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Time.timeScale = Time.timeScale == 0 ? 1 : 0;
            isPause = Time.timeScale == 0;
            pauseImage.gameObject.SetActive(isPause);
        }
    }

    private void Awake()
    {
        var objs = FindObjectsOfType<GameManager>();

        if (objs.Length > 1)
        {
            gameObject.SetActive(false);
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    private void OnEnable()
    {
        ScoreManager.OnScoreChanged += UpdateScore;
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
        LifeManager.OnGameOver += OnGameOver;
    }

    private void OnDisable()
    {
        ScoreManager.OnScoreChanged -= UpdateScore;
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
        LifeManager.OnGameOver -= OnGameOver;
    }

    private void UpdateScore()
    {
        scoreTextComponent.text = $"Score: {scoreManager.GetTotalScore()}";
    }

    private void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        if (isGameOver)
        {
            HideCanvas();
            ShowTotalScore();
        }
    }

    private void OnGameOver()
    {
        isGameOver = true;
        sceneLoader.LoadGameOverScene();
    }

    private void HideCanvas()
    {
        mainCanvas.enabled = false;
    }

    private void ShowTotalScore()
    {
        var canvases = FindObjectsOfType<Canvas>();
        var canvas = canvases.First(c => c.name == "EndCanvas");
        var textComponent = canvas.GetComponentsInChildren<TextMeshProUGUI>().First(t => t.name == "Total Score Text");
        textComponent.text = $"total score: {scoreManager.GetTotalScore()}";
    }
}
