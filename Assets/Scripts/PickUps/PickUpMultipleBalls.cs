﻿using UnityEngine;

public class PickUpMultipleBalls : Pickup
{
    [SerializeField]
    int ballNumber = 2;

    private void Start()
    {
        
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Pad")
        {
            var ball = FindObjectOfType<Ball>();
            for(int i=0; i< ballNumber; i++)
            {
                var newBall = Instantiate(ball);
                newBall.LaunchBall();
            }

            Destroy(gameObject);
        }
    }
}
