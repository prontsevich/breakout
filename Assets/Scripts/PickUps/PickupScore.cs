﻿using UnityEngine;

public class PickupScore : Pickup
{
    [SerializeField]
    int Score;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Pad")
        {
            var scoreManager = FindObjectOfType<ScoreManager>();
            scoreManager.AddScore(Score);
        }
    }
}
