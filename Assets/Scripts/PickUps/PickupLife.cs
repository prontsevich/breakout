﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupLife : Pickup
{
    [SerializeField]
    int lifesCount = 1;

    LifeManager lifeManager;
    void Start()
    {
        lifeManager = FindObjectOfType<LifeManager>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Pad")
        {
            if (lifesCount > 0)
            {
                lifeManager.AddLife(lifesCount);
            }
            else
            {
                lifeManager.WasteLife(lifesCount);
            }

        }
    }
}
