﻿using UnityEngine;

public class Pad : MonoBehaviour
{
    [SerializeField]
    float minX = 1.35f;

    [SerializeField]
    float maxX = 14.65f;

    private GameManager gameManager;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!gameManager.isPause)
        {
            var mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            var newX = Mathf.Clamp(mousePosition.x, minX, maxX);
            var position = new Vector2(newX, transform.position.y);

            transform.position = position;
        }
    }
}
