﻿using System.Linq;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    private Block[] blocks;
    private int blocksCount;

    private SceneLoader sceneLoader;

    public void RemoveBlock()
    {
        blocksCount--;
        if (blocksCount <= 0)
        {
            Time.timeScale = 0.3f;
            StartCoroutine(sceneLoader.LoadNextSceneDelay());
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        blocks = FindObjectsOfType<Block>();
        blocksCount = blocks.Where(b => !b.IsUnbreakable()).Count();

        sceneLoader = FindObjectOfType<SceneLoader>();
        Time.timeScale = 1;
    }
}
