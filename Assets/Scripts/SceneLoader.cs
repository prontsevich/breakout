﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public void LoadNextScene()
    {
        var currentScene = SceneManager.GetActiveScene();
        var nextSceneIndex = currentScene.buildIndex + 1 < SceneManager.sceneCountInBuildSettings ? currentScene.buildIndex + 1 : 0;
        SceneManager.LoadScene(nextSceneIndex);
    }

    public IEnumerator LoadNextSceneDelay()
    {
        yield return new WaitForSecondsRealtime(2);
        LoadNextScene();
    }

    public void LoadGameOverScene()
    {
        SceneManager.LoadScene(SceneManager.sceneCountInBuildSettings - 1);
    }
}
