﻿using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    public delegate void ScoreAction();
    public static event ScoreAction OnScoreChanged;

    private int totalScore;

    public void ResetScore()
    {
        totalScore = 0;
    }

    public void AddScore(int value)
    {
        totalScore += value;

        OnScoreChanged?.Invoke();
    }

    public int GetTotalScore()
    {
        return totalScore;
    }
}
