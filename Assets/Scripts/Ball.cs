﻿using System.Collections;
using UnityEngine;

public class Ball : MonoBehaviour
{
    [SerializeField]
    float speed;

    Pad pad;
    Vector3 ballOffset;
    bool isLaunched;
    Rigidbody2D rigidbody;
    private AudioSource audioSource;

    public IEnumerator ResetBall()
    {
        yield return new WaitForSeconds(1);
        isLaunched = false;
    }

    public void LaunchBall()
    {
        //float randomX = Random.Range(-10f, 10f);
        //var newSpeed = new Vector2(randomX, 10);
        
        var newSpeed = new Vector2(0, 10);
        rigidbody.velocity = newSpeed.normalized * speed;
        isLaunched = true;
    }

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        audioSource = GetComponent<AudioSource>();
    }

    // Start is called before the first frame update
    private void Start()
    {
        pad = FindObjectOfType<Pad>();
        ballOffset = transform.position - pad.transform.position;
    }

    // Update is called once per frame
    private void Update()
    {
        if (!isLaunched)
        {
            LockBallToPad();
            CheckLaunchBall();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Pad")
        {
            CorrectVelocity();
        }

        audioSource.Play();
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        var newSpeed = rigidbody.velocity;
        rigidbody.velocity = newSpeed.normalized * speed;
    }

    private void CheckLaunchBall()
    {
        if (Input.GetMouseButtonDown(0))
        {
            LaunchBall();
        }
    }

    private void LockBallToPad()
    {
        transform.position = pad.transform.position + ballOffset;
    }

    private void OnDrawGizmos()
    {
        if (Application.isPlaying)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(transform.position, transform.position + (Vector3)rigidbody.velocity);
        }
    }

    private void CorrectVelocity()
    {
        var padCollider = pad.GetComponent<BoxCollider2D>();
        var spriteRenderer = pad.GetComponent<SpriteRenderer>();

        float padWidth = (padCollider.size.x * pad.transform.localScale.x) / 2;
        //float padWidth = spriteRenderer.bounds.size.x / 2;
        float padPositionX = pad.transform.position.x;
        float ballPositionX = transform.position.x;

        float distance = ballPositionX - padPositionX;
        float percent = distance / padWidth;

        float newXSpeed = percent * 1;
        Vector2 newSpeed = new Vector2(newXSpeed, 1);
        rigidbody.velocity = newSpeed.normalized * speed;
    }
}
